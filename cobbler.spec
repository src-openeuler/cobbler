%global tftpboot_dir %{_sharedstatedir}/tftpboot
%global apache_webconfigdir %{_sysconfdir}/httpd/conf.d
%global vendor_lower `echo %{_vendor}|tr 'A-Z' 'a-z'`

Name:       cobbler
Version:    3.2.0
Release:    4
Summary:    Boot server configurator
URL:        https://cobbler.github.io/
License:    GPLv2+
Source0:    https://github.com/cobbler/cobbler/archive/v%{version}/%{name}-%{version}.tar.gz
BuildArch:  noarch

Patch9000:  huawei-adapt-vendor.patch
Patch9001:  huawei-repair-switch-condition-error.patch
Patch6000:  fix-Give-root-RW-permissions-to-var-lib-cobbler-web.ss.patch
Patch9002:  bugfix-change-permission-with-web.ss.patch
Patch6001:  backport-Fix-package-building-with-Sphinx.patch

BuildRequires:  system-release
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-coverage
BuildRequires:  python%{python3_pkgversion}-distro
BuildRequires:  python%{python3_pkgversion}-future
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-netaddr
BuildRequires:  python%{python3_pkgversion}-cheetah
BuildRequires:  python%{python3_pkgversion}-sphinx
BuildRequires:  python%{python3_pkgversion}-yaml
BuildRequires:  python%{python3_pkgversion}-simplejson
BuildRequires:  python%{python3_pkgversion}-requests
BuildRequires:  systemd

Requires(post):     systemd
Requires(preun):    systemd
Requires(postun):   systemd

Requires:       httpd
Requires:       tftp-server
Requires:       createrepo 
Requires:       file
Requires:       rsync
Requires:       xorriso
Requires:       dnf-plugins-core
Requires:       /sbin/service
Requires:       python%{python3_pkgversion}-cheetah
Requires:       python%{python3_pkgversion}-dns
Requires:       python%{python3_pkgversion}-future
Requires:       python%{python3_pkgversion}-mod_wsgi
Requires:       python%{python3_pkgversion}-netaddr
Requires:       python%{python3_pkgversion}-yaml
Requires:       python%{python3_pkgversion}-requests
Requires:       python%{python3_pkgversion}-simplejson
Requires:       python%{python3_pkgversion}-tornado
Requires:       python%{python3_pkgversion}-distro
Requires:       python%{python3_pkgversion}-ldap3
Requires:       genisoimage
Requires:       cobbler-web = %{version}-%{release}

Recommends:     bash-completion
Recommends:     syslinux
Recommends:     grub2-efi-x64
Recommends:     grub2-efi-aa64
Recommends:     logrotate

Obsoletes:      cobbler-nsupdate < 3.0.99
Provides:       cobbler-nsupdate = %{version}-%{release}

%description
Cobbler is a network install server. Cobbler supports PXE, ISO virtualized installs, and re-installing existing Linux
machines. The last two modes use a helper tool, 'koan', that integrates with cobbler. Cobbler's advanced features
include importing distributions from DVDs and rsync mirrors, kickstart templating, integrated yum mirroring, and
built-in DHCP/DNS Management. Cobbler has a XML-RPC API for integration with other applications.

%package -n cobbler-web
Summary:        Web interface for Cobbler
Requires:       cobbler = %{version}-%{release}
Requires:       python%{python3_pkgversion}-django
Requires:       python%{python3_pkgversion}-mod_wsgi
Requires:       mod_ssl
Requires(post): coreutils
Requires(post): sed

%description -n cobbler-web
Web interface for Cobbler that allows visiting
http://server/cobbler_web to configure the install server.

%prep
%autosetup -p1

sed -i "s/generic_os/%{_vendor}/g" distro_build_configs.sh
sed -i "s/generic_os/%{_vendor}/g" config/cobbler/distro_signatures.json
sed -i "s/generic_lower_os/%{vendor_lower}/g" cobbler/actions/check.py
sed -i "s/generic_lower_os/%{vendor_lower}/g" cobbler/utils.py
sed -i "s/generic_lower_os/%{vendor_lower}/g" config/cobbler/distro_signatures.json 
sed -i "s/generic_lower_os/%{vendor_lower}/g" cobbler/tftpgen.py
sed -i "s/generic_lower_os/%{vendor_lower}/g" cobbler/autoinstallgen.py
sed -i "s/generic_lower_os/%{vendor_lower}/g" cobbler/actions/buildiso.py

%build
. ./distro_build_configs.sh

# Check distro specific variables for consistency
[ "${DOCPATH}" != %{_mandir} ] && echo "ERROR: DOCPATH: ${DOCPATH} does not match %{_mandir}"

# [ "${ETCPATH}" != "/etc/cobbler" ]
# [ "${LIBPATH}" != "/var/lib/cobbler" ]
[ "${LOGPATH}" != %{_localstatedir}/log ] && echo "ERROR: LOGPATH: ${LOGPATH} does not match %{_localstatedir}/log"
[ "${COMPLETION_PATH}" != %{_datadir}/bash-completion/completions ] && \
    echo "ERROR: COMPLETION: ${COMPLETION_PATH} does not match %{_datadir}/bash-completion/completions"

[ "${WEBROOT}" != %{apache_dir} ] && echo "ERROR: WEBROOT: ${WEBROOT} does not match %{apache_dir}"
[ "${WEBCONFIG}" != %{apache_webconfigdir} ] && echo "ERROR: WEBCONFIG: ${WEBCONFIG} does not match %{apache_webconfigdir}"
[ "${TFTPROOT}" != %{tftpboot_dir} ] && echo "ERROR: TFTPROOT: ${TFTPROOT} does not match %{tftpboot_dir}"

%py3_build

%install
. ./distro_build_configs.sh
%py3_install

# cobbler
rm %{buildroot}%{_sysconfdir}/cobbler/cobbler.conf

mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
mv %{buildroot}%{_sysconfdir}/cobbler/cobblerd_rotate %{buildroot}%{_sysconfdir}/logrotate.d/cobblerd

# Create data directories in tftpboot_dir
mkdir -p %{buildroot}%{tftpboot_dir}/{boot,etc,grub/system{,_link},images{,2},ppc,pxelinux.cfg,s390x}

# systemd
mkdir -p %{buildroot}%{_unitdir}
mv %{buildroot}%{_sysconfdir}/cobbler/cobblerd.service %{buildroot}%{_unitdir}

# cobbler-web
rm %{buildroot}%{_sysconfdir}/cobbler/cobbler_web.conf

# ghosted files
touch %{buildroot}%{_sharedstatedir}/cobbler/web.ss

# rm test file
rm -rf %{buildroot}%{_datarootdir}/cobbler/tests

%pre
if [ $1 -ge 2 ]; then
    # package upgrade: backup configuration
    DATE=$(date "+%%Y%%m%%d-%%H%%M%%S")
    if [ ! -d "%{_sharedstatedir}/cobbler/backup/upgrade-${DATE}" ]; then
        mkdir -p "%{_sharedstatedir}/cobbler/backup/upgrade-${DATE}"
    fi
    for i in "config" "snippets" "templates" "triggers" "scripts"; do
        if [ -d "%{_sharedstatedir}/cobbler/${i}" ]; then
            cp -r "%{_sharedstatedir}/cobbler/${i}" "%{_sharedstatedir}/cobbler/backup/upgrade-${DATE}"
        fi
    done
    if [ -d %{_sysconfdir}/cobbler ]; then
        cp -r %{_sysconfdir}/cobbler "%{_sharedstatedir}/cobbler/backup/upgrade-${DATE}"
    fi
fi

%post
%systemd_post cobblerd.service

%preun
%systemd_preun cobblerd.service

%postun
%systemd_postun_with_restart cobblerd.service

%post -n cobbler-web
# Change the SECRET_KEY option in the Django settings.py file
# required for security reasons, should be unique on all systems
# Choose from letters and numbers only, so no special chars like ampersand (&).
RAND_SECRET=$(head /dev/urandom | tr -dc 'A-Za-z0-9!' | head -c 50; echo '')
sed -i -e "s/SECRET_KEY = ''/SECRET_KEY = \'$RAND_SECRET\'/" %{_datadir}/cobbler/web/settings.py

%files
%license COPYING
%doc AUTHORS.in README.md
%doc docs/developer-guide.rst docs/quickstart-guide.rst docs/installation-guide.rst
%config(noreplace) %{_sysconfdir}/cobbler
%config(noreplace) %{_sysconfdir}/logrotate.d/cobblerd
%config(noreplace) %{apache_webconfigdir}/cobbler.conf
%{_bindir}/cobbler
%{_bindir}/cobbler-ext-nodes
%{_bindir}/cobblerd
%{_sbindir}/tftpd.py
%{_sbindir}/fence_ipmitool
%{_datadir}/bash-completion/
%dir %{_datadir}/cobbler
%{_datadir}/cobbler/bin
%{python3_sitelib}/cobbler/
%{python3_sitelib}/cobbler-*.egg-info
%{_unitdir}/cobblerd.service
%{tftpboot_dir}/*
/var/www/cobbler
%config(noreplace) %{_sharedstatedir}/cobbler
%exclude %{_sharedstatedir}/cobbler/web.ss
%exclude %{_sharedstatedir}/cobbler/webui_sessions
%{_localstatedir}/log/cobbler

%files -n cobbler-web
%license COPYING
%doc AUTHORS.in README.md
%config(noreplace) %{apache_webconfigdir}/cobbler_web.conf
%attr(-,apache,apache) %{_datadir}/cobbler/web
%ghost %attr(0660,apache,root) %{_sharedstatedir}/cobbler/web.ss
%dir %attr(700,apache,root) %{_sharedstatedir}/cobbler/webui_sessions
%attr(-,apache,apache) /var/www/cobbler_webui_content/

%changelog
* Fri Aug 11 2023 zhangpan <zhangpan103@h-partners.com> - 3.2.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DES:fix build fail

* Thu Mar 09 2023 sunhai <sunhai10@huawei.com> - 3.2.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DES:Change permission with web.ss 

* Tue Feb 28 2023 sunhai <sunhai10@huawei.com> - 3.2.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add install depend

* Sat May 29 2021 liuxin <liuxin264@huawei.com> - 3.2.0-1
- Package init


